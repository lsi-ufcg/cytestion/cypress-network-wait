function getNetworkWait({ userTimeout = 30000, verbose = false }) {
  function networkWait(definedTimeout) {
    const MINIMUM_STATIC_WAIT = 500;
    const RECURSIVE_STATIC_WAIT = 200;

    const options = { timeout: definedTimeout ?? userTimeout };
    const retryTotal = Math.ceil(options.timeout / RECURSIVE_STATIC_WAIT);

    cy.wait(MINIMUM_STATIC_WAIT);
    function checkPendingAPICount(retryCount) {
      return cy.wrap(null, { log: false }).then(() => {
        const pendingRequests = () => Cypress.env("pendingAPICount"); 

        const isFirstExecution = retryCount === 0;
        if (verbose || isFirstExecution) {
          cy.log(
            `Waiting for pending web requests: ${pendingRequests()}`
          );
        }

        const allRequestsCompleted = pendingRequests() === 0;
        if (allRequestsCompleted) {
          cy.log("All requests completed!");
        } else if (retryCount >= retryTotal) {
          cy.log("Wait timeout reached, proceeding!");
        } else {
          return cy
            .wait(RECURSIVE_STATIC_WAIT, { log: false })
            .then(() => checkPendingAPICount(++retryCount));
        }
      });
    };
    return checkPendingAPICount(0).then(() => {
      cy.wait(MINIMUM_STATIC_WAIT);
    });
  };
  return networkWait;
};

module.exports = { getNetworkWait };
