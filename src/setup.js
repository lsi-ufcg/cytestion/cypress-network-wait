const {
  setupCypressInterception,
  plantWaitUntilSomeRequestOccurs,
} = require("./interception");

function setupInterception({ apiHosts }) {
  Cypress.on("url:changed", (newUrl) => {
    plantWaitUntilSomeRequestOccurs();
    console.log("newUrl", newUrl);
  });

  beforeEach(() => {
    setupCypressInterception(apiHosts);
  });
}

module.exports = {
  setupInterception,
};
