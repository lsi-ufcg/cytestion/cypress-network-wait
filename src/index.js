const { setupInterception } = require("./setup");

const { getNetworkWait } = require("./network-wait");

function registerCommand({ userTimeout, verbose }) {
  Cypress.Commands.add(
    "networkWait",
    { prevSubject: "optional" },
    getNetworkWait({ userTimeout, verbose })
  );
}

function setupNetworkWait({ timeout: userTimeout, verbose, apiHosts } = {}) {
  setupInterception({ apiHosts });
  registerCommand({ userTimeout, verbose });
}

module.exports = {
  setupNetworkWait,
};
