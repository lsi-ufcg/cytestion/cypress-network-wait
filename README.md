# Cypress Network Wait

Cypress Network Wait is a command that enhances Cypress testing capabilities by implementing a "wait for network" principle, ensuring stable and reliable tests even in dynamic web environments. It efficiently handles asynchronous network requests, allowing seamless synchronization with your application's state changes.

## Installation

```sh
# Using npm
npm install cypress-network-wait

# Using yarn
yarn add cypress-network-wait
```

## Getting started

Integrating Cypress Network Wait into your Cypress tests is straightforward. Simply import the `setupNetworkWait` function and call it in your `e2e.js` file.

```js
import { setupNetworkWait } from "cypress-network-wait";

setupNetworkWait();
```

You can also provide a timeout in miliseconds, a flag for the verbose mode or a personal configuration of your API hosts
```js
import { setupNetworkWait } from "cypress-network-wait";

setupNetworkWait({ timeout: 10000, verbose: true, apiHosts: ["*"] });
```

## How it works

Cypress Network Wait operates on the principle of intercepting and counting network requests. It is composed of two main functions:

> `setupInterception()`, that sets up the request interceptors **before each** test case.

> `registerCommand({ userTimeout, verbose })`, that adds the `cy.networkWait()` command to cypress.


It's important to keep in mind that `setupInterception` initializes a beforeEach statement internally. Be aware of any potential impact on your system.

## Usage

Add `cy.networkWait()` into your Cypress tests after any actions that trigger state changes in your application.

```js
describe("example to-do app", () => {
  beforeEach(() => {
    cy.visit("https://example.cypress.io/todo");

    // Recommended usage after `cy.visit()` 
    // for guaranteed state change
    cy.networkWait();
  });

  it("can add new todo items", () => {
    const newItem = "Feed the cat";
    cy.get("[data-test=new-todo]").type(`${newItem}{enter}`);

    // Also use after actions changing
    // on-screen elements' state
    cy.networkWait();
    cy.get(".todo-list li")
      .should("have.length", 3)
      .last()
      .should("have.text", newItem);
  });
});
```

## Example Output

In regular mode, Cypress Network Wait provides only the total amount of pending requests.

![Cypress run log showing cy.networkWait() output in regular mode](https://gitlab.com/lsi-ufcg/cytestion/cypress-network-wait/-/raw/main/assets/cypress_regular_execution.png?ref_type=heads)

In verbose mode, it shows the request count decreasing progressively.

![Cypress run log showing cy.networkWait() output in verbose mode](https://gitlab.com/lsi-ufcg/cytestion/cypress-network-wait/-/raw/main/assets/cypress_verbose_execution.png?ref_type=heads)


## Contribute

Found a bug or have a feature request? We welcome contributions from the community. Visit our [GitLab repository](https://gitlab.com/lsi-ufcg/cytestion/cypress-network-wait) to get started.


## License

Cypress Network Wait is licensed under the [MIT License](https://opensource.org/licenses/MIT). Feel free to use, modify, and distribute it according to your needs.